<footer>

    <div class="footer-message">

        <div class="container">
    
            <div class="message-content">
    
                <div class="title">
                    <i class="fas fa-piggy-bank"></i>
                    <h3>Gostaria de ajudar o projeto?</h3>
                </div>
    
                <p>Lorem ipsum dolor sit amet curabitur eu dapibus felis. Morbi ut dolor ut mi ultricies lobortis.<br>Morbi et viverra turpis. Donec cursus ornare purus nec porttitor.</p>
    
                <a href="#" class="btn btn-primary">Download Minutes Now</a>
    
            </div>
    
        </div>
    
    </div>

    <div class="main-footer">

        <div class="container">
    
            <div class="row">
                
                <div class="col-12 col-md-8 column-1">
					<div class="footer-logo"><a href="index.html">Pequi</a></div>
					<p>Lorem ipsum dolor sit amet consectetur adipiscing elit rhoncus tempus perdiet risus sit amet volutpat.</p>
					<ul class="footer-social">
						<li><a href="#"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#"><i class="fab fa-dribbble"></i></a></li>
						<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
						<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
						<li><a href="#"><i class="fab fa-instagram"></i></a></li>
					</ul>
                </div>
                
				<div class="col-12 col-md-4 column-2">
					<h4>Related Links</h4>
					<div class="row">
						<div class="col-md-6">
							<ul class="footer-list">
								<li><a href="#">About Us</a></li>
								<li><a href="#">Testimonials</a></li>
								<li><a href="#">Our Team</a></li>
								<li><a href="#">Contact Us</a></li>
							</ul>
						</div>
						<div class="col-md-6">
							<ul class="footer-list">
								<li><a href="#">Register</a></li>
								<li><a href="#">Forum</a></li>
								<li><a href="#">Affiliate</a></li>
								<li><a href="#">FAQ</a></li>
							</ul>
						</div>
					</div>
                </div>
                
			</div>
    
        </div>

    </div>

</footer>